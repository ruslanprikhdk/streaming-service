import React, { useEffect, useContext } from 'react';
import {useState} from 'react';
import styles from './Login.module.css';
import { Link } from 'react-router-dom';
import {getAuth, signInWithEmailAndPassword} from 'firebase/auth';
import { AuthContext } from '../context';

const Login = ({history}) => {

  const {isAuth, setIsAuth} = useContext(AuthContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      history.push('/main');
    }
  }, [])


  const onLogin = () => {
    setLoading(true);
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        setIsAuth(true);
        localStorage.setItem('token', userCredential._tokenResponse.idToken);
        localStorage.setItem('email', email);
        history.push('/main');
      })
      .catch(e => alert(e.message))
      .finally(() => setLoading(false))
  }

  return (
    <div className={styles.container}>
      <div className={styles.form_container}>
        <div className={styles.input_container}>
          <label className={styles.label}>Email</label>
          <input
            value={email}
            onChange={e => setEmail(e.target.value)} 
            type='email'
            name='email'
            className={styles.email_input}
          />
        </div>
        <div className={styles.input_container}>
          <label className={styles.label}>Password</label>
          <input
            value={password}
            onChange={e => setPassword(e.target.value)} 
            type='password'
            name='password'
            className={styles.password_input}
          />
        </div>
        <div>
          <button 
            onClick={onLogin}
            className={styles.button}>
            {loading ? 'Logging you in...' : 'Log In'}
          </button>
        </div>
        <div className={styles.signup_container}>          
            <span>Don't have an account? Please, sign up </span> 
            <Link to='/signup'>here</Link>          
        </div>
      </div>      
    </div>
  );
};

export default Login;