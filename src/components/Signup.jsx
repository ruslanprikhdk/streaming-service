import React from 'react';
import {getAuth, createUserWithEmailAndPassword, updateProfile} from 'firebase/auth';
import styles from './Signup.module.css';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import app from '../firebase';
import { getFirestore, setDoc, doc } from 'firebase/firestore';

const Signup = ({ history }) => {

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [preferredGenres, setPreferredGenres] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      history.push('/main');
    }
  }, [])

  const onSignup = () => {
    setLoading(true);
    const auth = getAuth();

    createUserWithEmailAndPassword(auth, email, password)
      .then(() => {
        updateProfile(auth.currentUser, {displayName: name})
          .then(() => {
            history.push('/')
            handleNewUser();
          })
          .catch((e) => alert(e.message))
      }).catch((e) => alert(e.message))
      .finally(() => setLoading(false))

  }

  const db = getFirestore(app)

  // const [dtbUsers, setDtbUsers] = useState([]);

  // useEffect(() => 
  //   onSnapshot(collection(db, 'users'), (snapshot) => 
  //     setDtbUsers(snapshot.docs.map(doc => doc.data()))
  //   ),
  //    []);

  const handleNewUser = async () => {
    const docRef = doc(db, 'users', name);
    const payload = {name: name, preferred_genres: preferredGenres, email: email, likedMovies: [], sharedMovies: [], friends: []}
    await setDoc(docRef, payload);
  }
  return (
    <div className={styles.container}>
      <div className={styles.form_container}>
        <div className={styles.input_container}>
          <label className={styles.label}>Username</label>
          <input
            value={name}
            onChange={e => setName(e.target.value)}
            type='text'
            name='username'
            className={styles.input}
          />
        </div>
        <div className={styles.input_container}>
          <label className={styles.label}>Email</label>
          <input
            value={email}
            onChange={e => setEmail(e.target.value)} 
            type='email'
            name='email'
            className={styles.input}
          />
        </div>
        
        <div className={styles.input_container}>
          <label className={styles.label}>Password</label>
          <input 
            value={password}
            onChange={e => setPassword(e.target.value)}
            type='password'
            name='password'
            className={styles.input}
          />
        </div>
        <div className={styles.input_container}>
          <label className={styles.label}>Preferred genres</label>
          <input 
            value={preferredGenres}
            onChange={e => setPreferredGenres(e.target.value)}
            type='text'
            name='genres'
            className={styles.input}
          />
        </div>
        <div>
          <button 
            className={styles.button} 
            onClick={onSignup}>
              {loading ? 'Creating user...' : 'Sign Up'}
          </button>
        </div>
        <div className={styles.signup_container}>          
            <span>Already have an account? Please, log in </span> 
            <Link to='/'>here</Link>
          
        </div>
      </div>
      
    </div>
  );
};

export default Signup;