import React, {useContext} from 'react';
import {Link} from 'react-router-dom';
import styles from './Navbar.module.css';
import {getAuth, signOut} from 'firebase/auth';
import { AuthContext } from '../context';

const Navbar = ({history}) => {

  const auth = getAuth();
  const {isAuth, setIsAuth, currentUser, setCurrentUser} = useContext(AuthContext);

  const logout = () => {
    signOut(auth)
      .then(() => {    
        setIsAuth(false);
        setCurrentUser(null);
        history.push('/');       
        localStorage.removeItem('token');
        localStorage.removeItem('email');        
        
      })
      .catch((e) => alert(e.message))
  }

  return (
    <div className={styles.navbar}>
      <div className='container'>
        <nav className={styles.container}>
          <h3>
            <span>Hello, <span>{currentUser ? currentUser.name : ''}. How are you today?</span><input
              type='button'
              onClick={logout}
              value='Log Out'/>
            </span>
          </h3>
          <ul className={styles.nav_list}>
            <li>
                <Link to='/'>MAIN</Link>
            </li>           
            <li>
                <Link to='/myprofile'>MY PROFILE</Link>
            </li>
            <li>
                <Link to='/users'>USERS</Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;