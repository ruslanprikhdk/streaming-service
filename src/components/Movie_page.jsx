import React, {useState, useEffect, useContext} from 'react';
import styles from './Movie_page.module.css';
import img from '../img/no-image.jpg';
import Navbar from '../components/Navbar';
import { getFirestore, setDoc, doc } from 'firebase/firestore';
import app from '../firebase';
import { AuthContext } from '../context';

const Movie_page = (props) => {


  const {currentUser, friendsList, usersList} = useContext(AuthContext);
  const db = getFirestore(app);
  const singleMovie = props.location.movieProps;
  const history = props.history;
  const [likedMovies, setLikedMovies] = useState([]);
  const [likedMovie, setLikedMovie] = useState(false);

useEffect(() => {
    let singleMovieArr = [singleMovie];
    let currentUserLikedMovies;
    let updLikedMovies;

        currentUserLikedMovies = [currentUser.likedMovies];
        if (currentUserLikedMovies.length === 0) {
          setLikedMovies([singleMovie]);
        } else if (checkMovieInDtb(currentUserLikedMovies[0], singleMovie)) {
          updLikedMovies = currentUserLikedMovies[0];
          setLikedMovies(updLikedMovies);
          setLikedMovie(true);
        } else {
          updLikedMovies = currentUserLikedMovies[0].concat(singleMovieArr);
          setLikedMovies(updLikedMovies);
        }             
  }, [])

  function checkMovieInDtb (dtb, movie) {
    let check = false;
    for (let i = 0; i < dtb.length; i++) {
      if(dtb[i].id === movie.id) {
        check = true;
      }
    }
      
      return check;
    
  }

  const removeTags = (text) => {
    if (text === null || text === '') {
      return false;
    } else {
      text = text.toString();
    }
    return text.replace(/(<([^>]+)>)/gi,'');
  }

  const addLike = async (e) => {
    const docRef = doc(db, 'users', currentUser.name);
      e.target.style.backgroundColor = 'red';
      setLikedMovie(true);
    await setDoc(docRef, {likedMovies: likedMovies}, {merge: true});    
  }
  
  const removeLike = async (e) => {
    const docRef = doc(db, 'users', currentUser.name);
    e.target.style.backgroundColor = 'green';
    setLikedMovie(false);
    const updLikedMovies = (removeMovieFromLiked(singleMovie));
    await setDoc(docRef, {likedMovies: updLikedMovies}, {merge: true});    
  }


  function removeMovieFromLiked (currentMovie) {
    const updLikedMovies = likedMovies.filter(movie => {
      return movie.name !== currentMovie.name; 
    });
    return updLikedMovies;
  }

  const shareMovie = (e) => {
    e.preventDefault();
    e.target.style.backgroundColor = 'red';
    e.target.value = 'Sharing...';
    setTimeout(() => {
      e.target.style.backgroundColor = 'green';
      e.target.value = 'Share';
    }, 1000);
    if(friendsList.length > 0) {
      friendsList.forEach(item => {
        return shareMovieWithFriend(item)
      });
    }   
  }
  
  const shareMovieWithFriend = async (friend) => {
    let friendSharedMovies;
    let finalFriendSharedMovies;
    friendSharedMovies = usersList.filter(item => item.name === friend)[0].sharedMovies;
    

    if (friendSharedMovies.length > 0 && !checkMovieInDtb(friendSharedMovies, singleMovie)) {
      finalFriendSharedMovies = ([...friendSharedMovies, singleMovie]);
    } else if (friendSharedMovies.length > 0) {
      finalFriendSharedMovies = friendSharedMovies;
    } else {
      finalFriendSharedMovies = ([singleMovie]);
    }
    
    const docRef = doc(db, 'users', friend);
    await setDoc(docRef, {sharedMovies: finalFriendSharedMovies}, {merge: true});
  }

  return (
    <div>
      <Navbar history={history}/>
      <div className={styles.container}>
          <img 
              src={singleMovie.image ? singleMovie.image.medium : img} 
              alt={singleMovie.name}
            />
            <div className={styles.single_movie_info}>
              <h1>{singleMovie.name}</h1>
              {singleMovie.genres && singleMovie.genres.map(genre => (
                <span key={genre} className={styles.single_movie_info_genre}>{genre}</span>
              ))}
              <p>
                <strong>Status:</strong> {singleMovie.status && singleMovie.status}
              </p>
              <p>
                <strong>Rating:</strong> {singleMovie.rating ? singleMovie.rating.average : 'No rating'}
              </p>
              <p>
                <strong>Official Site:</strong> {singleMovie.officialSite ? (<a href={singleMovie.officialSite} target='_blank' rel='noreferrer'>{singleMovie.officialSite}</a>) : 'No official site'}
              </p>
              <p>{singleMovie.summary && removeTags(singleMovie.summary)}</p>
              <input type='button' className={styles.share_btn} onClick={shareMovie} value='Share'/>
              <button type='button' className={likedMovie ? styles.unlike_btn : styles.like_btn} onClick={likedMovie ? removeLike : addLike}>{likedMovie ? 'Unlike' : 'Like'}</button>
            </div>
        </div>
    </div>
  );
};

export default Movie_page;