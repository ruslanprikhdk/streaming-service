import React, {useState, useContext} from 'react';
import {Link} from 'react-router-dom';
import styles from './ListItem.module.css';
import { getFirestore, setDoc, doc } from 'firebase/firestore';
import app from '../firebase';
import { AuthContext } from '../context';

const ListItem = ({image, name, rating, id, genres, movie, likeStatus, likedMovies}) => {

  const {currentUser, friendsList, usersList} = useContext(AuthContext);
  const [currentFriendSharedMovies, setCurrentFriendSharedMovies] = useState([]);

  const email = localStorage.getItem('email');
  const db = getFirestore(app);

  const addLike = async (e) => {
    e.preventDefault();    
    e.target.style.backgroundColor = 'red';
    e.target.textContent = 'Unlike';
    const docRef = doc(db, 'users', currentUser.name);
    await setDoc(docRef, {likedMovies: [...likedMovies, movie]}, {merge: true});    
  }
  
  const removeLike = async (e) => {
    e.preventDefault();    
    e.target.style.backgroundColor = 'green';
    e.target.textContent = 'Like';
    const docRef = doc(db, 'users', currentUser.name);
    const updLikedMovies = (removeMovieFromLiked(movie));
    await setDoc(docRef, {likedMovies: updLikedMovies}, {merge: true});    
  }

  function removeMovieFromLiked (currentMovie) {
    const updLikedMovies = likedMovies.filter(item => {
      return item.name !== name; 
    });
    return updLikedMovies;
  }

  const shareMovie = (e) => {
    e.preventDefault();
    e.target.style.backgroundColor = 'red';
    e.target.value = 'Sharing...';
    setTimeout(() => {
      e.target.style.backgroundColor = 'green';
      e.target.value = 'Share';
    }, 1000);
    if(friendsList.length > 0) {
      friendsList.forEach(item => {
        return shareMovieWithFriend(item)
      });
    }   
  }
  
  const shareMovieWithFriend = async (friend) => {
    let friendSharedMovies;
    let finalFriendSharedMovies;
    friendSharedMovies = usersList.filter(item => item.name === friend)[0].sharedMovies;
    

    if (friendSharedMovies.length > 0 && !checkMovieInDtb(friendSharedMovies, movie)) {
      finalFriendSharedMovies = ([...friendSharedMovies, movie]);
    } else if (friendSharedMovies.length > 0) {
      finalFriendSharedMovies = friendSharedMovies;
    } else {
      finalFriendSharedMovies = ([movie]);
    }
    
    setCurrentFriendSharedMovies(finalFriendSharedMovies);
    const docRef = doc(db, 'users', friend);
    await setDoc(docRef, {sharedMovies: finalFriendSharedMovies}, {merge: true});
  }

  function checkMovieInDtb (dtb, movie) {
    let check = false;
    for (let i = 0; i < dtb.length; i++) {
      if(dtb[i].id === movie.id) {
        check = true;
      }
    }      
      return check;    
  }

  return (
    <Link 
      to={{
        pathname: `/movies/${id}`,
        movieProps: movie}} 
      className={styles.container} 
    >
      <img src={image} alt={name}/>
      <div className={styles.movie_info}>
        <h4 className={styles.movie_info_title}>{name}</h4>
        <h4 className={styles.movie_info_genres}>{genres.map(genre => (
          <span key={genre} className={styles.single_movie_info_genre}>{genre} </span>
          ))}</h4>
        <h4 className={styles.movie_info_rating}>{rating}</h4>
        <input type='button' className={styles.share_btn} onClick={shareMovie} value='Share'/>
        <button type='button' onClick={likeStatus ? removeLike : addLike} className={likeStatus ? styles.like_btn : styles.unlike_btn}>{likeStatus ? 'Unlike' : 'Like'}</button>
      </div>
    </Link>
  );
};

export default ListItem;