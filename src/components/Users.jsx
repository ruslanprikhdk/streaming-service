import React, {useEffect, useState, useContext} from 'react';
import styles from './Users.module.css';
import app from '../firebase';
import { getFirestore, setDoc, doc } from 'firebase/firestore';
import Navbar from '../components/Navbar';
import { AuthContext } from '../context';

const Users = ({history}) => {

  const {currentUser, usersList, friendsList} = useContext(AuthContext);
  const email = localStorage.getItem('email');
  const db = getFirestore(app);
  
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      history.push('/');
    }
  }, []) 

  const addFriend = async (e) => {
    let friendName = (e.target.parentNode.firstChild.firstChild.innerText).split(' ')[1];
    e.target.style.backgroundColor = 'red';
    e.target.textContent = 'Unfriend';
    const docRef = doc(db, 'users', currentUser.name);
    await setDoc(docRef, {friends: [...friendsList, friendName]}, {merge: true});   
  }

  const removeFriend = async (e) => {
    let friendName = (e.target.parentNode.firstChild.firstChild.innerText).split(' ')[1];
    e.target.style.backgroundColor = 'green';
    e.target.textContent = 'Add Friend';
    const docRef = doc(db, 'users', currentUser.name);
    const updFriendsDtb = (removeFriendFromList(friendName));
    await setDoc(docRef, {friends: updFriendsDtb}, {merge: true});   
  }

  function removeFriendFromList (friend) {
    const updFriendsList = friendsList.filter(item => {
      return item !== friend; 
    });
    return updFriendsList;
  }  

  return (
    <div>
      <Navbar history={history}/>
      <div className={styles.container}>
        {usersList.map((user, index) => (
          <div key={index} className={styles.user_container}>
            <div className={styles.user_info}>
              <p>Name: {user.name}</p>
              <p>Preferences: {user.preferred_genres}</p>              
            </div>
            <div className={styles.likedMovies_info}>
              <p>Liked Movies:</p>
              <div className={styles.user_data_container}>{user.likedMovies && user.likedMovies.map(movie => (
                <div className={styles.likedMovies_container}>
                  <img src={movie.image.medium} alt={movie.name}/>
                  <div className={styles.movie_info}>
                    <h4 className={styles.movie_info_title}>{movie.name}</h4>
                    <h4 className={styles.movie_info_rating}>{movie.rating.average}</h4>
                  </div>
                </div>
              ))}</div>
            </div>
            
            <div className={styles.likedMovies_info}>
              <p>Movies Shared With User:</p>
              <div className={styles.user_data_container}>{user.sharedMovies && user.sharedMovies.map(movie => (
              <div className={styles.likedMovies_container}>
                <img src={movie.image.medium} alt={movie.name}/>
                <div className={styles.movie_info}>
                  <h4 className={styles.movie_info_title}>{movie.name}</h4>
                  <h4 className={styles.movie_info_rating}>{movie.rating.average}</h4>
                </div>
              </div>
            ))}</div>
            </div>
            <button className={friendsList.includes(user.name) ? styles.unfriend_btn :  styles.friend_btn} onClick={friendsList.includes(user.name) ? removeFriend : addFriend}>{friendsList.includes(user.name) ? 'Unfriend' :  'Add Friend'}</button>            
          </div>
          )
        )}
      </div>      
    </div>
  );
};

export default Users;