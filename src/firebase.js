// // Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import {getFirestore} from 'firebase/firestore'; 
// // TODO: Add SDKs for Firebase products that you want to use
// // https://firebase.google.com/docs/web/setup#available-libraries

// // Your web app's Firebase configuration
// const firebaseConfig = {
//   apiKey: "AIzaSyBLXmm1Oa3FOjebR3BhWN_LaigU3KVK9XU",
//   authDomain: "streaming-service-87319.firebaseapp.com",
//   projectId: "streaming-service-87319",
//   storageBucket: "streaming-service-87319.appspot.com",
//   messagingSenderId: "249576090246",
//   appId: "1:249576090246:web:062d698167784fe16d7f07"
// };

// // Initialize Firebase
// const app = initializeApp(firebaseConfig);
// export default getFirestore();

const firebaseConfig = {
  apiKey: "AIzaSyBLXmm1Oa3FOjebR3BhWN_LaigU3KVK9XU",
  authDomain: "streaming-service-87319.firebaseapp.com",
  projectId: "streaming-service-87319",
  storageBucket: "streaming-service-87319.appspot.com",
  messagingSenderId: "249576090246",
  appId: "1:249576090246:web:062d698167784fe16d7f07"
};

const app = initializeApp(firebaseConfig);
export default app;