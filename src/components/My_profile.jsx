import React, {useEffect, useContext} from 'react';
import styles from './My_profile.module.css';
import app from '../firebase';
import { getFirestore, setDoc, doc } from 'firebase/firestore';
import Navbar from '../components/Navbar';
import { AuthContext } from '../context';

const My_profile = ({history}) => {
  const {currentUser, usersList, friendsList, setFriendsList, likedMovies, sharedMovies} = useContext(AuthContext);
  const email = localStorage.getItem('email');
  const db = getFirestore(app);
  
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      history.push('/');
    }
  }, []) 


  const removeFriend = async (e) => {
    let friendName = (e.target.parentNode.firstChild.innerText);
    const docRef = doc(db, 'users', currentUser.name);
    const updFriendsDtb = (removeFriendFromList(friendName));
    setFriendsList(updFriendsDtb);
    await setDoc(docRef, {friends: updFriendsDtb}, {merge: true});   
  }

  function removeFriendFromList (friend) {
    const updFriendsList = friendsList.filter(item => {
      return item !== friend; 
    });
    return updFriendsList;
  }  

  return (
    <div>
      <Navbar history={history} user={currentUser}/>
      <div className={styles.container}>

          <div className={styles.profile_container}>
            <div className={styles.user_info}>
              <p>Name: {currentUser? currentUser.name : ''}</p>
              <p>Preferences: {currentUser ? currentUser.preferred_genres : ''}</p>
              <p>My Dearest Friends:</p>
              <div className={styles.friends_container}>{friendsList.length > 0 ? friendsList.map(friend => (
                <p><span>{friend}</span><button className={styles.unfriend_btn} onClick={removeFriend}>Unfriend</button></p>
              )) : <p>No friends so far</p>} </div>             
            </div>

            <div className={styles.likedMovies_info}>
              <p>Liked Movies:</p>
              <div className={styles.user_data_container}>{likedMovies.length > 0 ? likedMovies.map(movie => (
                <div className={styles.likedMovies_container}>
                  <img src={movie.image.medium} alt={movie.name}/>
                  <div className={styles.movie_info}>
                    <h4 className={styles.movie_info_title}>{movie.name}</h4>
                    <h4 className={styles.movie_info_rating}>{movie.rating.average}</h4>
                  </div>
                </div>
              )): <p>No liked movies so far</p>}</div>
            </div>
            
            <div className={styles.likedMovies_info}>
              <p>Movies Shared With User:</p>
              <div className={styles.user_data_container}>{sharedMovies.length > 0 ?sharedMovies.map(movie => (
              <div className={styles.likedMovies_container}>
                <img src={movie.image.medium} alt={movie.name}/>
                <div className={styles.movie_info}>
                  <h4 className={styles.movie_info_title}>{movie.name}</h4>
                  <h4 className={styles.movie_info_rating}>{movie.rating.average}</h4>
                </div>
              </div>
            )): <p>No shared movies so far</p>}</div>
            </div>            
          </div>
          
        
      </div>      
    </div>
  );
};

export default My_profile;