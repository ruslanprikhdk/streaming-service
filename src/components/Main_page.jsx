import React, { useEffect, useMemo, useState, useContext } from 'react';
import ListItem from '../components/ListItem';
import axios from 'axios';
import img from '../img/no-image.jpg';
import styles from './Main_page.module.css';
import app from '../firebase';
import { getFirestore, onSnapshot, collection } from 'firebase/firestore';
import { getAuth, onAuthStateChanged } from '@firebase/auth';
import Navbar from "../components/Navbar";
import MyInput from './UI/MyInput';
import { AuthContext } from '../context';

const Main_page = ({history}) => {


  useEffect(() => {
    const token = localStorage.getItem('token');    
    if (!token) {
      history.push('/');
    }
  }, [])

  const email = localStorage.getItem('email');

  const {isAuth, setIsAuth, currentUser, setCurrentUser, setUsersList, setFriendsList, likedMovies, setLikedMovies, setSharedMovies} = useContext(AuthContext);
  
  const db = getFirestore(app);

  useEffect(() => {
    const db = getFirestore(app);
    const auth = getAuth();
    onAuthStateChanged(auth, user => {
      setCurrentUser(user);
      setIsAuth(true);
    });
    onSnapshot(collection(db, 'users'), (snapshot) => {
      const usersDtb = snapshot.docs.map(doc => doc.data());
      setUsersList(usersDtb.filter(item => item.email !== email));
      setCurrentUser(usersDtb.filter(item => item.email === email)[0]);
      setFriendsList(usersDtb.filter(item => item.email === email)[0].friends);
      setLikedMovies(usersDtb.filter(item => item.email === email)[0].likedMovies);
      setSharedMovies(usersDtb.filter(item => item.email === email)[0].sharedMovies);
      }                
    )
  }, [])

  let pages = 3;
  const [shows, setShows] = useState([]);
  const [selectedSort, setSelectedSort] = useState('');
  const [searchQueryByName, setSearchQueryByName] = useState('');
  const [searchQueryByGenres, setSearchQueryByGenres] = useState('');
  const [searchBy, setSearchBy] = useState('');
  
  const auth = getAuth();
  const user = auth.currentUser;

  useEffect(() => {
    let cleanupFunction = false;
    let reallyFinalData = [];
      async function getShows (pageNumber, shows) {     
      const response = await axios.get(`https://api.tvmaze.com/shows?page=${pageNumber}`)     
      let updList = await shows.concat(response.data);
      reallyFinalData.push(...response.data);
      setShows(updList);
      }
        for (let i = 0; i < pages; i++) {
            getShows(i, reallyFinalData)
        }
    return () => cleanupFunction = true;
  }, [])

  const sortedMovies = useMemo(() => {
    if(selectedSort) {
      [...shows].sort((a, b) => {
        if(shows[0].rating.average < 1) {
          return Number(b.rating.average) - Number(a.rating.average);
        } else {
          return Number(a.rating.average) - Number(b.rating.average);
        }    
      })
    }
    return shows;
  }, [selectedSort, shows])

  const sortedAndSearchedMovies = useMemo(() => {
    switch (searchBy) { 
      case 'name':
        if (searchQueryByGenres) {
          return sortedMovies.filter(movie => movie.genres.join(',').toLowerCase().includes(searchQueryByGenres)).filter(movie => movie.name.toLowerCase().includes(searchQueryByName));
        } else {
          return sortedMovies.filter(movie => movie.name.toLowerCase().includes(searchQueryByName));
        }       
      case 'genres':
        if(searchQueryByName) {
          return sortedMovies.filter(movie => movie.name.toLowerCase().includes(searchQueryByName)).filter(movie => movie.genres.join(',').toLowerCase().includes(searchQueryByGenres));
        } else {
          return sortedMovies.filter(movie => movie.genres.join(',').toLowerCase().includes(searchQueryByGenres));
        }
      default:
        return sortedMovies.filter(movie => movie.name.toLowerCase().includes(searchQueryByName)); 
    }
  }, [searchQueryByName, searchQueryByGenres, sortedMovies])

  const likedMoviesIds = likedMovies.map((item => item.id));

  const sortMoviesAsc = () => {
    let sortedMovies = [...shows].sort((a, b) => Number(a.rating.average) - Number(b.rating.average))
    setShows(sortedMovies);
  }

  const sortMoviesDesc = () => {
    let sortedMovies = [...shows].sort((a, b) => Number(b.rating.average) - Number(a.rating.average))
    setShows(sortedMovies);
  }

  const clearSearchFields = () => {
    setSearchQueryByGenres('');
    setSearchQueryByName('');
  }

  return (
    <div className='main_page'>
      <Navbar history={history}/>
      <div className={styles.search_panel_container}>
        <div className={styles.search_by_name_container}>
          <p>Search by name:</p>
          <MyInput
          value={searchQueryByName}
          placeholder='Please, enter value to search'
          onChange={e => {
          setSearchQueryByName(e.target.value);
          setSearchBy('name');
          }}
          />
        </div>
        <div className={styles.clear_button_container}>
          <button className={styles.clear_button} onClick={clearSearchFields}>Clear Search Fields</button>
        </div>        
        <div className={styles.search_by_genres_container}>
          <p>Search by genres:</p>
          <MyInput
            value={searchQueryByGenres}
            placeholder='Please, enter value to search'
            onChange={e => {
              setSearchQueryByGenres(e.target.value);
              setSearchBy('genres');      
            }}
          />
        </div>
        <div className={styles.sort_btns_container}>          
          <button onClick={sortMoviesDesc}>Sort by rating (desc)</button>
          <button onClick={sortMoviesAsc}>Sort by rating (asc)</button>
        </div>
      </div>
      <div className={styles.container}>
        {sortedAndSearchedMovies.map(movie => (
          <div key={movie.id}>
              <ListItem
                likedMovies={likedMovies}
                likeStatus={likedMoviesIds.includes(movie.id) ? true : false}
                movie={movie}
                genres={movie.genres}
                id={movie.id}
                image={movie.image ? movie.image.medium : img}
                name={movie.name}
                rating={movie.rating.average ? movie.rating.average : 'No rating was provided'}
              />
          </div>    
        ))}
      </div>

    </div>    
  );
};

export default Main_page;