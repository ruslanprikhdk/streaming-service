import {Route, BrowserRouter, Switch} from 'react-router-dom';
import { useState } from 'react';
import Login from "./components/Login";
import Signup from "./components/Signup";
import MyProfile from "./components/My_profile";
import MainPage from './components/Main_page';
import MoviePage from './components/Movie_page';
import Users from './components/Users';
import { AuthContext } from './context';

function App() {
  const email = localStorage.getItem('email');
  const [isAuth, setIsAuth] = useState(false);
  const [currentUser, setCurrentUser] = useState(null);
  const [usersList, setUsersList] = useState([]);
  const [friendsList, setFriendsList] = useState([]);
  const [likedMovies, setLikedMovies] = useState([]);
  const [sharedMovies, setSharedMovies] = useState([]);
  
  return (
    <AuthContext.Provider value={{
      isAuth,
      setIsAuth,
      currentUser,
      setCurrentUser,
      usersList,
      setUsersList,
      friendsList,
      setFriendsList,
      likedMovies,
      setLikedMovies,
      sharedMovies,
      setSharedMovies
    }}>
      <BrowserRouter>
        <div className='container'>
          <Switch>
            <Route exact path='/' component={Login}/>
            <Route path='/signup' component={Signup}/>
            <Route path='/main' component={MainPage}/>
            <Route path='/users' component={Users}/>
            <Route path='/movies/:id' component={MoviePage}/>
            <Route path='/myprofile' component={MyProfile}/>
          </Switch>
        </div>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}


export default App;
